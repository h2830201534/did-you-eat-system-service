package com.work.mywork;

import com.work.mywork.utils.JwtUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MyworkApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void token(){
        String token = JwtUtils.getToken("abc");
        System.out.println(token);

        Boolean decode = JwtUtils.decode(token);
        System.out.println(decode);
    }

}
