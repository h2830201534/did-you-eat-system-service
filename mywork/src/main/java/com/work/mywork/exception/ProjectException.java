package com.work.mywork.exception;

import com.work.mywork.common.Code;
import com.work.mywork.common.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailSendException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class ProjectException {

    @ExceptionHandler(MailSendException.class)
    public Result smtpAddressFailedException(MailSendException exception){
        log.info("邮件异常");
        return new Result(Code.USER_ERROR,"邮件发送失败");
    }


}
