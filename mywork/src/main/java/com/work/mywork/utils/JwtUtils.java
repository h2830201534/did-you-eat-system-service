package com.work.mywork.utils;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.Date;

@Slf4j
// jwt加密与解密
public class JwtUtils {

    private static final long EXPERT = 60 * 60 * 24 * 1000; // 加密时间
    private static final String SECRET = "eyJzdWIiOiJHRVQgVE9LRU4YW0xIjoxNTI4MjIxNTA0MDQwMjYzNjgxLCJwYXJhbTIiOiIyMjIyMjIifQ";

    /**
     * 生成token
     * @param id
     * @return
     */
    public  static String getToken(String id){
        log.info("token生成中。。。");
        String token = Jwts.builder()
                .setSubject("getToken")
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + EXPERT))
                .claim("id",id)
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();

        return token;
    }

    /**
     * 验证token时效
     * @param token
     * @return
     */
    public static Boolean decode(String token){

        log.info("token验证中。。。");
        if (StringUtils.isEmpty(token)){
            log.info("token为空");
            return false;
        }

        try{
            // 解密出现异常就说明是无效token
            Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token);
        }catch (Exception exception){
            log.info("token验证失败");
            return false;
        }

        return true;
    }




}
