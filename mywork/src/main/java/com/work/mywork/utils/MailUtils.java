package com.work.mywork.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;


@Component
@Slf4j
public  class MailUtils {

    @Value("${spring.mail.username}")
    private  String adminEmail;

    @Resource
    private   JavaMailSender javaMailSender;

    @Resource
    private   TemplateEngine templateEngine;


    /**
     * 邮件发送
     * @param activeUrl
     * @param userEmail
     * @return
     */
    public  boolean send(String activeUrl,String userEmail){
        log.info("激活地址：{}",activeUrl);
        log.info("用户邮箱：{}",userEmail);
        try {
            // 创建邮件对象
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage,true);
            // 设置邮件主题
            mimeMessageHelper.setSubject("欢迎使用吃了吗系统服务 --- 账号激活");
            // 设置邮件的发送者
            mimeMessageHelper.setFrom(adminEmail);
            // 设置邮件的接收者
            mimeMessageHelper.setTo(userEmail);
            // 设置邮件的发送时间
            mimeMessageHelper.setSentDate(new Date());
            // 创建上下文环境对象
            Context context = new Context();
            context.setVariable("activeUrl",activeUrl);
            // 将内容放入模板中
            String text = templateEngine.process("active.html",context);

            // 内容装载
            mimeMessageHelper.setText(text,true);

            // 邮件发送
            javaMailSender.send(mimeMessage);
        } catch (MessagingException e){
            return false;
        }

        return true;
    }


}
