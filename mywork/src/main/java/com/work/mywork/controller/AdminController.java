package com.work.mywork.controller;


import com.work.mywork.common.Code;
import com.work.mywork.common.Result;
import com.work.mywork.entity.Admin;
import com.work.mywork.service.impl.AdminServiceImpl;
import com.work.mywork.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
@Slf4j
public class AdminController {

    @Autowired
    AdminServiceImpl adminService;

    /**
     * 管理员登录接口
     * @param admin
     * @return
     */
    @PostMapping("/login")
    public Result login(@RequestBody Admin admin){
        System.out.println(admin);
        log.info("登录方法");

        Admin findAdmin = adminService.getById(admin.getId());

        // 1.查询账号是否存在
        if (findAdmin == null){
            return new Result(Code.LOGIN_ERROR,"账号未注册");
        }

        // 2.密码验证
        String password = DigestUtils.md5DigestAsHex(admin.getPassword().getBytes(StandardCharsets.UTF_8));
        System.out.println(password);
        System.out.println(findAdmin.getPassword());
        if (!password.equals(findAdmin.getPassword())){
            return new Result(Code.LOGIN_ERROR,"密码有误");
        }

        // 3.登录成功，获取token返回
        String token = JwtUtils.getToken(admin.getId());
        Map<String,String> map = new HashMap<>();
        map.put("token",token);
        map.put("name", findAdmin.getName());

        return new Result(Code.LOGIN_SUCCESS,"登录成功",map);
    }





}
