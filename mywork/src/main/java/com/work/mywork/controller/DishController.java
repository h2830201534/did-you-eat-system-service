package com.work.mywork.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.work.mywork.common.Code;
import com.work.mywork.common.Result;
import com.work.mywork.dto.DishDto;
import com.work.mywork.entity.Category;
import com.work.mywork.entity.Dish;
import com.work.mywork.service.impl.CategoryServiceImpl;
import com.work.mywork.service.impl.DishServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/dish")
@Slf4j
public class DishController {

    @Autowired
    DishServiceImpl dishService;
    @Autowired
    CategoryServiceImpl categoryService;

    /**
     * 菜品分页查询
     * @param page
     * @param pageSize
     * @param keyword
     * @return
     */
    @GetMapping
    public Result getPageData(int page,int pageSize,String keyword){
        log.info("分类分页查询中。。。");
        log.info("页号：{} --- 数量：{} --- 关键词：{}",page,pageSize,keyword);

        Page<Dish> dishPage = new Page<>(page, pageSize);
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();

        // 分类名称模糊查询
        if (!StringUtils.isEmpty(keyword)){
            log.info("菜品分类名称反向查询中。。。");
            LambdaQueryWrapper<Category> categoryLambdaQueryWrapper = new LambdaQueryWrapper<>();
            categoryLambdaQueryWrapper.like(Category::getName,keyword);
            List<Map<String, Object>> maps = categoryService.listMaps(categoryLambdaQueryWrapper);
            for (Map<String, Object> map : maps) {
                queryWrapper.eq(Dish::getIsDeleted,0).eq(Dish::getCategoryId,map.get("id")).or();
            }
        }


        queryWrapper
                .eq(Dish::getIsDeleted,0)  // 查询未被删除的
                .like(!StringUtils.isEmpty(keyword),Dish::getName,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),Dish::getCreateUser,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),Dish::getUpdateUser,keyword)
                .or()
                .orderByAsc(Dish::getSort)
                .orderByDesc(Dish::getCreateTime);

        dishService.page(dishPage,queryWrapper);

        // 菜品类名查询
        Page<DishDto> dishDtoPage = new Page<>(); // 保存查询最终结果

        BeanUtils.copyProperties(dishPage,dishDtoPage,"records"); //先保存其它的属性

        List<Dish> records = dishPage.getRecords(); // 单独处理数据列

        List<DishDto> dishDtoList = new ArrayList<>(); // 临时存储

        for (Dish record : records) {
            DishDto dishDto = new DishDto();

            BeanUtils.copyProperties(record,dishDto); // 其余数据迁移

            Long categoryId = record.getCategoryId(); // 查询类名
            Category category = categoryService.getById(categoryId);
            dishDto.setCategoryName(category.getName());

            dishDtoList.add(dishDto);
        }

        dishDtoPage.setRecords(dishDtoList);

        Map<String,Object> map = new HashMap<>();
        map.put("dataList", dishDtoPage.getRecords());
        map.put("total", dishDtoPage.getTotal());

        return new Result(Code.DISH_SUCCESS,"查询成功",map);
    }

    /**
     * 菜品添加、编辑
     * @param dish
     * @return
     */
    @PostMapping
    public Result add(@RequestBody Dish dish){

        log.info(dish+"");
        // 判断是否为编辑操作
        if (dish.getId() != null){
            log.info("菜品编辑");
            // 判断该名称是否被删除，删除则进行恢复
            LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
            dishLambdaQueryWrapper
                    .eq(Dish::getName,dish.getName())
                    .eq(Dish::getIsDeleted,1);

            Dish one = dishService.getOne(dishLambdaQueryWrapper);
            if (one != null){
                one.setIsDeleted(0);
                dishService.updateById(one);
                return new Result(Code.DISH_SUCCESS,"菜品【"+one.getName()+"】恢复成功");
            }

            // 判断新的菜品名称是否存在
            dishLambdaQueryWrapper.clear();
            dishLambdaQueryWrapper
                    .eq(Dish::getIsDeleted,0)
                    .eq(Dish::getName,dish.getName())
                    .notLike(Dish::getId,dish.getId()); // 不对自己进行查询
            Dish serviceOne = dishService.getOne(dishLambdaQueryWrapper);
            if (serviceOne != null){
                return new Result(Code.DISH_ERROR,"菜品【"+serviceOne.getName()+"】已经存在");
            }

            dishService.updateById(dish);
            return  new Result(Code.DISH_SUCCESS,"菜品修改成功");
        }

        // 判断分类名称是否存在
        LambdaQueryWrapper<Category> categoryLambdaQueryWrapper = new LambdaQueryWrapper<>();
        categoryLambdaQueryWrapper
                .eq(Category::getId,dish.getCategoryId())
                .eq(Category::getIsDeleted,0);
        int count = categoryService.count(categoryLambdaQueryWrapper);
        log.info(count+"");
        if (count == 0){
            return new Result(Code.DISH_ERROR,"菜品不存在");
        }

        // 判断菜品是否存在
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Dish::getName,dish.getName());
        Dish serviceOne = dishService.getOne(queryWrapper);

        // 菜品如果存在，检测是否被删除
        if (serviceOne!=null && serviceOne.getIsDeleted() == 1){
            // 被删除，恢复该菜品
            serviceOne.setIsDeleted(0);
            dishService.updateById(serviceOne);
            return new Result(Code.DISH_SUCCESS,"菜品【"+dish.getName()+"】恢复成功");
        }

        // 判断菜品名是否冲突
        if (serviceOne != null){
            return new Result(Code.DISH_ERROR,"菜品【"+dish.getName()+"】已经存在");
        }

        // 添加
        dishService.save(dish);

        return new Result(Code.DISH_SUCCESS,"添加成功");
    }

    /**
     * 菜品删除
     * @param id
     * @return
     */
    @DeleteMapping
    public Result del(Long id) {
        log.info(id + "");

        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Dish::getId, id);

        Dish serviceOne = dishService.getOne(queryWrapper);

        serviceOne.setIsDeleted(1);
        boolean flag = dishService.updateById(serviceOne);
        if (!flag){
            return new Result(Code.DISH_ERROR, "删除异常");
        }
        return new Result(Code.DISH_SUCCESS, "菜品【" + serviceOne.getName() + "】删除成功");
    }

    /**
     * 菜品状态修改
     * @param dish
     * @return
     */
    @PutMapping
    public Result setStatus(@RequestBody Dish dish) {
        log.info("菜品状态更改为：{}",dish.getStatus());

        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Dish::getId, dish.getId());

        Dish serviceOne = dishService.getOne(queryWrapper);

        serviceOne.setStatus(dish.getStatus());
        boolean flag = dishService.updateById(serviceOne);
        if (!flag){
            return new Result(Code.DISH_ERROR, "状态更改时发送异常");
        }
        return new Result(Code.DISH_SUCCESS, "菜品【" + serviceOne.getName() + "】状态更改成功");
    }

}
