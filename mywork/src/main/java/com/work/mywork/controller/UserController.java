package com.work.mywork.controller;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.work.mywork.common.Code;
import com.work.mywork.common.Result;
import com.work.mywork.entity.User;
import com.work.mywork.entity.UserTmp;
import com.work.mywork.service.UserTmpService;
import com.work.mywork.service.impl.UserServiceImpl;
import com.work.mywork.utils.MailUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
@Slf4j
public class UserController {

    @Autowired
    UserServiceImpl userService;
    @Autowired
    UserTmpService userTmpService;

    // 注入工具类
    @Autowired
    MailUtils mailUtils;

    /**
     * 用户管理 --- 分页查询
     * @param page
     * @param pageSize
     * @param keyword
     * @return
     */
    @GetMapping
    public Result getPageData(int page,int pageSize,String keyword){
        log.info("用户分页查询中。。。");
        log.info("页号：{} --- 数量：{} --- 关键词：{}",page,pageSize,keyword);

        Page<User> userPage = new Page<>(page, pageSize);
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.like(!StringUtils.isEmpty(keyword),User::getUserid,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),User::getUsername,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),User::getSex,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),User::getCreateUser,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),User::getUpdateUser,keyword);

        userService.page(userPage,queryWrapper);

        List<Object> userList = new ArrayList<>();

        for (User record : userPage.getRecords()) {
            Map<String,Object> map = new HashMap<>();
            map.put("userid",record.getUserid());
            map.put("username",record.getUsername());
            map.put("sex",record.getSex() == 1?"男":"女");
            map.put("status",record.getStatus());
            map.put("isActive",record.getIsActive() == 0 ? "未激活":"已激活");
            map.put("createUser",record.getCreateUser());
            map.put("updateUser",record.getUpdateUser());
            map.put("createTime",record.getCreateTime());
            map.put("updateTime",record.getUpdateTime());
            map.put("lastTime",record.getLastTime());
            userList.add(map);
        }
        Map<String,Object> map = new HashMap<>();
        map.put("dataList",userList);
        map.put("total",userPage.getTotal());
        return  new Result(Code.USER_SUCCESS,"查询成功",map);
    }

    /**
     * 用户管理 --- 状态修改
     * @param user
     * @return
     */
    @PostMapping
    public Result setStatus(@RequestBody User user){
        log.info("状态修改中。。。");
        boolean flag = userService.updateById(user);
        Integer status = user.getStatus();
        String userid = user.getUserid();
        log.info("账号：{}的状态修改为【{}】",userid,status);
        return flag
                ? new Result(Code.USER_SUCCESS, status==0? "账号【"+userid+"】已被锁定":"账号【"+userid+"】已解锁")
                : new Result(Code.USER_ERROR,"服务异常");
    }

    /**
     * 用户添加
     * @param user
     * @return
     */
    @PostMapping("/add")
    public Result registerUser(@RequestBody User user){
        log.info("用户添加中");

        // 1.判断用户是否已经注册
        User serviceById = userService.getById(user.getUserid());
        if (serviceById != null){
            return new Result(Code.USER_ERROR,"该账号已注册");
        }

        // 2.放入用户临时表中存储，等待激活
        UserTmp userTmp = new UserTmp();
        userTmp.setUserid(user.getUserid());
        userTmp.setPassword(user.getPassword());
        userTmp.setUsername(user.getUsername());
        userTmp.setSex(user.getSex());
        userTmp.setActiveTime(LocalDateTime.now().plusMinutes(5)); //五分钟以内有效
        // 使用工具包通过雪花算法生成确认码
        long code = IdUtil.getSnowflakeNextId();
        userTmp.setConfirmCode(code);

        userTmpService.save(userTmp);

        // 3.向用户发送激活文件
        String url = " http://***************/tmp?confirmCode=" + code;
        boolean flag = mailUtils.send(url, user.getUserid());
        if (flag){
            log.info("邮件发送成功");
            return new Result(Code.USER_SUCCESS,"用户注册成功,请在5分钟内通过邮件激活");
        }else {
            log.info("邮件发送失败");
            return new Result(Code.USER_ERROR,"邮件发送失败");
        }

    }

}
