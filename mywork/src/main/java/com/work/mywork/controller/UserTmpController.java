package com.work.mywork.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.work.mywork.common.BaseContext;
import com.work.mywork.common.Code;
import com.work.mywork.common.Result;
import com.work.mywork.entity.User;
import com.work.mywork.entity.UserTmp;
import com.work.mywork.service.impl.UserServiceImpl;
import com.work.mywork.service.impl.UserTmpServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/tmp")
@Slf4j
public class UserTmpController {
    @Autowired
    UserTmpServiceImpl userTmpService;

    @Autowired
    UserServiceImpl userService;

    @GetMapping
    public Result confirm(Long confirmCode, HttpServletResponse response) throws IOException {
        log.info("激活中。。。。");
        log.info("确认码：{}", confirmCode);
        // 在用户注册临时表中查找唯一的确认码
        LambdaQueryWrapper<UserTmp> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserTmp::getConfirmCode, confirmCode);
        UserTmp serviceOne = userTmpService.getOne(queryWrapper);
        if (serviceOne != null){

            // 判断是否超时
            boolean after = LocalDateTime.now().isAfter(serviceOne.getActiveTime());
            if (after){
                return new Result(Code.USER_ERROR,"激活超时，链接已失效");
            }

            // 确认码一致，激活成功
            log.info("确认码一致，激活成功");
            User user = new User();
            user.setUserid(serviceOne.getUserid());
            String password = DigestUtils.md5DigestAsHex(serviceOne.getPassword().getBytes(StandardCharsets.UTF_8));
            user.setUsername(serviceOne.getUsername());
            user.setPassword(password);
            user.setSex(serviceOne.getSex());
            user.setIsActive(1);
            user.setLastTime(LocalDateTime.now());
            BaseContext.setId(serviceOne.getUserid());
            userService.save(user);

            // 删除临时表中的数据
            userTmpService.remove(queryWrapper);
            response.sendRedirect("http://**************/");
        }
        return new Result(Code.USER_ERROR,"激活失败");
    }
}
