package com.work.mywork.controller;

import com.work.mywork.common.Code;
import com.work.mywork.common.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@Slf4j
@RequestMapping("/file")
// TODO 文件上传下载实现
public class FileController {

    @Value("${image.path}")
    private String basePath;

    /**
     * 必须使用POST方法上传
     * 接收参数名必须和前端发送的名字一样
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public Result upload(MultipartFile file){
        log.info("图片文件存储中。。。");

        // 获取原始文件名
        String originalFilename = file.getOriginalFilename();

        // 截取文件后缀
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        log.info("文件后缀：{}",suffix);

        // 随机生成文件名
        UUID imgId = UUID.randomUUID();

        // 生成的文件名
        String filename = imgId+suffix;
        log.info("生成的文件名：{}",filename);

        // 将该文件存储到目录中
        File dir = new File(basePath);

        // 判断目录是否存在
        if (!dir.exists()){
            dir.mkdir();
        }

        // 将文件从临时目录转存至指定目录下
        try{
            file.transferTo(new File(basePath + filename));
        }catch (IOException ex){
            ex.getMessage();
        }

        // 将生成的文件名返给前端
        Map<String,String> map = new HashMap<>();
        map.put("filename",filename);
        return new Result(Code.DISH_SUCCESS,"图片文件上传成功",map);

    }


    /**
     * 必须使用GET方法下载
     * 接收参数名必须和前端发送的名字一样
     * @param filename
     * @param response
     */
    @GetMapping("/download")
    public void download(String filename, HttpServletResponse response){
        log.info("图片下载中。。。");
        log.info(filename);
        try{
            // 通过输入流读取指定的文件
            FileInputStream fileInputStream = new FileInputStream(basePath+ filename);

            // 通过输出流写入到浏览器中进行展示
            ServletOutputStream servletOutputStream = response.getOutputStream();
            //设置返回的数据类型
            response.setContentType("image/jpeg");

            int len = 0;
            byte bytes [] = new byte[1024];
            while ( (len = fileInputStream.read(bytes)) != -1){
                servletOutputStream.write(bytes,0,len);
                servletOutputStream.flush();
            }

            fileInputStream.close();
            servletOutputStream.close();

        }catch (FileNotFoundException e){
            e.getMessage();
        }catch (IOException e){
            e.getMessage();
        }

    }

}
