package com.work.mywork.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.work.mywork.common.Code;
import com.work.mywork.common.Result;
import com.work.mywork.entity.Category;
import com.work.mywork.entity.Dish;
import com.work.mywork.service.impl.CategoryServiceImpl;
import com.work.mywork.service.impl.DishServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/api/category")
public class CategoryController {

    @Autowired
    CategoryServiceImpl categoryService;

    @Autowired
    DishServiceImpl dishService;

    /**
     * 分类分页查询
     * @param page
     * @param pageSize
     * @param keyword
     * @return
     */
    @GetMapping
    public Result getPageData(int page,int pageSize,String keyword){
        log.info("分类分页查询中。。。");
        log.info("页号：{} --- 数量：{} --- 关键词：{}",page,pageSize,keyword);

        Page<Category> categoryPage = new Page<>(page, pageSize);
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper
                .eq(Category::getIsDeleted,0)  // 查询未被删除的
                .like(!StringUtils.isEmpty(keyword),Category::getName,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),Category::getSort,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),Category::getCreateUser,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),Category::getUpdateUser,keyword)
                .orderByAsc(Category::getSort)
                .orderByDesc(Category::getCreateTime);

        categoryService.page(categoryPage,queryWrapper);

        Map<String,Object> map = new HashMap<>();
        map.put("dataList",categoryPage.getRecords());
        map.put("total",categoryPage.getTotal());

        return new Result(Code.CATEGORY_SUCCESS,"查询成功",map);
    }

    /**
     * 分类添加、编辑
     * @param category
     * @return
     */
    @PostMapping
    public Result categoryAdd(@RequestBody Category category){
        log.info("分类添加/编辑中。。。");

        // 判断是添加还是编辑操作,id不为空则是编辑操作
        if (category.getId() != null){
            LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();

            // 判断编辑的名称是否被删除，如果被删除则恢复，当前的分类状态被转为删除
            queryWrapper
                    .eq(Category::getName,category.getName())
                    .eq(Category::getIsDeleted,1);
            Category one = categoryService.getOne(queryWrapper);
            if (one != null) {
                // 检测到是过往删除的分类，进行恢复
                one.setIsDeleted(0);
                categoryService.updateById(one);
                return  new Result(Code.CATEGORY_SUCCESS,"分类【"+category.getName()+"】恢复成功");
            }

            // 判断新的名称是否存在
            queryWrapper.clear();
            queryWrapper
                    .eq(Category::getIsDeleted,0)  // 查询未被删除的
                    .eq(Category::getName,category.getName())
                    .notLike(Category::getId,category.getId());
            Category serviceOne = categoryService.getOne(queryWrapper);
            if (serviceOne != null){
                return new Result(Code.CATEGORY_ERROR,"分类【"+category.getName()+"】已经存在");
            }
            categoryService.updateById(category);
            return  new Result(Code.CATEGORY_SUCCESS,"分类修改成功");
        }

        // 判断添加的分类名是否被逻辑删除，如果被删除则恢复它
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(Category::getName,category.getName())
                .eq(Category::getIsDeleted,1);

        Category categoryServiceOne = categoryService.getOne(queryWrapper);
        if (categoryServiceOne!=null){
            categoryServiceOne.setIsDeleted(0);
            categoryService.updateById(categoryServiceOne);
            return  new Result(Code.CATEGORY_SUCCESS,"分类【"+category.getName()+"】恢复成功");
        }

        // 判断分类名是否已经存在
        queryWrapper.clear();
        queryWrapper
                .eq(Category::getIsDeleted,0)  // 查询未被删除的
                .eq(Category::getName,category.getName());
        int count= categoryService.count(queryWrapper);
        if (count > 0){
            return new Result(Code.CATEGORY_ERROR,"分类【"+category.getName()+"】已经存在");
        }

        categoryService.save(category);

        return new Result(Code.CATEGORY_SUCCESS,"分类添加成功");
    }

    /**
     * 分类编辑前置查询
     * @param id
     * @return
     */
    @GetMapping("/one")
    public Result getOneData(Long id){
        Category category = categoryService.getById(id);
        System.out.println(category);

        Map<String,Object> map = new HashMap<>();
        map.put("name",category.getName());
        map.put("sort",category.getSort());

        return new Result(Code.CATEGORY_SUCCESS,"查询成功",map);
    }

    /**
     * 分类删除
     * @param id
     * @return
     */
    @DeleteMapping
    public Result delete(Long id){
        log.info("分类删除中。。。");

        Category category = categoryService.getById(id);

        // 判断该分类是否已经绑定了菜品，如果绑定了就不允许删除
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.eq(Dish::getCategoryId,id);
        int count = dishService.count(dishLambdaQueryWrapper);  // 注意，在这里不能用getOne()
        if (count > 0){
            return new Result(Code.CATEGORY_ERROR,"分类【"+category.getName()+"】已经绑定了菜品，无法删除!");
        }

        category.setIsDeleted(1);
        boolean flag = categoryService.updateById(category);
        if (!flag){
            return new Result(Code.CATEGORY_ERROR,"删除异常");
        }

        return new Result(Code.CATEGORY_SUCCESS,"分类【"+category.getName()+"】删除成功");
    }

    /**
     * 获取所有分类
     * @return
     */
    @GetMapping("/getallname")
    public Result getAllName(){

        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Category::getIsDeleted,0);

        List<Map<String, Object>> mapList = categoryService.listMaps(queryWrapper);

        return new Result(Code.CATEGORY_SUCCESS,"分类名称查询成功",mapList);

    }


}
