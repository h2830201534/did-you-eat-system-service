package com.work.mywork.dto;


import com.work.mywork.entity.Dish;
import lombok.Data;

@Data
public class DishDto extends Dish {
    private String categoryName;
}
