package com.work.mywork.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserTmp {
    private String userid;  // 注册邮箱
    private String username;
    private String password;
    private Integer sex;
    private Integer isActive; // 激活状态
    private Long confirmCode; // 确认码
    private LocalDateTime activeTime; // 有效时间
}
