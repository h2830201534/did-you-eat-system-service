package com.work.mywork.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.work.mywork.entity.Admin;

public interface AdminService extends IService<Admin> {
}
