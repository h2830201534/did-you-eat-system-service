package com.work.mywork.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.work.mywork.entity.Dish;

public interface DishService extends IService<Dish> {
}
