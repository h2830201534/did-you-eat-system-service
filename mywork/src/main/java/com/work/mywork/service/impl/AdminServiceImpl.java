package com.work.mywork.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.work.mywork.entity.Admin;
import com.work.mywork.mapper.AdminMapper;
import com.work.mywork.service.AdminService;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {
}
