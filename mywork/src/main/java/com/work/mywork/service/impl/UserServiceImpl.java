package com.work.mywork.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.work.mywork.entity.User;
import com.work.mywork.mapper.UserMapper;
import com.work.mywork.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
