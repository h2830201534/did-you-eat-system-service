package com.work.mywork.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.work.mywork.entity.UserTmp;
import com.work.mywork.mapper.UserTmpMapper;
import com.work.mywork.service.UserTmpService;
import org.springframework.stereotype.Service;

@Service
public class UserTmpServiceImpl extends ServiceImpl<UserTmpMapper, UserTmp> implements UserTmpService {
}
