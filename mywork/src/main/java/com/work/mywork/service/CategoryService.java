package com.work.mywork.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.work.mywork.entity.Category;

public interface CategoryService extends IService<Category> {
}
