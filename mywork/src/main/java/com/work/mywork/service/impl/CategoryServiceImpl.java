package com.work.mywork.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.work.mywork.entity.Category;
import com.work.mywork.mapper.CategoryMapper;
import com.work.mywork.service.CategoryService;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {
}
