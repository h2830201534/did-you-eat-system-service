package com.work.mywork.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.work.mywork.entity.Dish;
import com.work.mywork.mapper.DishMapper;
import com.work.mywork.service.DishService;
import org.springframework.stereotype.Service;

@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService{
}
