package com.work.mywork.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.work.mywork.entity.User;

public interface UserService extends IService<User> {
}
