package com.work.mywork.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.work.mywork.entity.UserTmp;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserTmpMapper extends BaseMapper<UserTmp> {
}
