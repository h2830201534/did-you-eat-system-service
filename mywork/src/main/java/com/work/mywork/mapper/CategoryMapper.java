package com.work.mywork.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.work.mywork.entity.Category;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}
