package com.work.mywork.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.work.mywork.entity.Admin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminMapper extends BaseMapper<Admin> {
}
