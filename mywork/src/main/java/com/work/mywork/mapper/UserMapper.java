package com.work.mywork.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.work.mywork.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
}
