package com.work.mywork.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.work.mywork.entity.Dish;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DishMapper extends BaseMapper<Dish> {
}
