package com.work.mywork.common;

// 公共字段设置
public class BaseContext {

    public static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static void setId(String id){
        threadLocal.set(id);
    }

    public static String getId(){
       return threadLocal.get();
    }

}
