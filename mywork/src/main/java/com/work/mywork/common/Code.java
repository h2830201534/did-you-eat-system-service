package com.work.mywork.common;


public class Code {

    public static final Integer LOGIN_SUCCESS = 10000;
    public static final Integer LOGIN_ERROR = 10001;

    public static final Integer USER_SUCCESS = 20000;
    public static final Integer USER_ERROR = 20001;

    public static final Integer CATEGORY_SUCCESS = 30000;
    public static final Integer CATEGORY_ERROR = 30001;
    public static final Integer DISH_SUCCESS = 40000;
    public static final Integer DISH_ERROR = 40001;
    public static final Integer TOKEN_ERROR = 401;


}
