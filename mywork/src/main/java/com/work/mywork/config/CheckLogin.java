package com.work.mywork.config;

import com.alibaba.fastjson.JSON;
import com.work.mywork.common.BaseContext;
import com.work.mywork.common.Code;
import com.work.mywork.common.Result;
import com.work.mywork.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "myfilter",value = "/*")
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CheckLogin implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        // 1.向下转型
        HttpServletRequest servletRequest =  (HttpServletRequest) request;
        HttpServletResponse servletResponse =  (HttpServletResponse) response;

        log.info("token:{}",servletRequest.getHeader("token"));

        // 解决跨域问题
        servletResponse.setHeader("Access-Control-Allow-Origin", servletRequest.getHeader("Origin"));
        servletResponse.setHeader("Access-Control-Allow-Methods", "*");
        servletResponse.setHeader("Access-Control-Allow-Headers", "*");
        servletResponse.setHeader("Access-Control-Allow-Credentials", "true");
        servletResponse.setHeader("Access-Control-Expose-Headers", "*");


        String requestURI = servletRequest.getRequestURI(); // 请求的uri
        log.info("本次请求的uri为：{}",requestURI);
        log.info("本次请求的方法为：{}",servletRequest.getMethod());

        // 操作者id
        String operator = servletRequest.getHeader("operator");
        BaseContext.setId(operator);

        // 2.定义放行url
        String urls [] = new String[]{
                "/api/login",
                "/file/upload",
                "/file/download",
                "/tmp"
        };

        // 3.路由匹配
        AntPathMatcher antPathMatcher = new AntPathMatcher();

        boolean flag = false;
        for (String url : urls) {
            if (antPathMatcher.match(requestURI, url)){
                flag = true;
                break;
            }
        }

        if (flag){
            log.info("路由匹配成功，放行");
            chain.doFilter(servletRequest, servletResponse);
            return;
        }

        log.info("需要对token进行验证");
        Boolean res = JwtUtils.decode(servletRequest.getHeader("token"));
        if (res){
            log.info("token验证成功~");
            chain.doFilter(servletRequest, servletResponse);
            return;
        }

        //token验证失败
        response.getWriter().write(JSON.toJSONString(new Result(Code.TOKEN_ERROR, "NOTLOGIN")));
        return;

    }
}
