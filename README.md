## 吃了吗系统服务后端设计





#### 一、数据库 tackout

管理员表

<img src="https://imgpp.com/s1/2022/06/10/image-20220610163533016.png" alt="image-20220610163533016.png" border="0">

用户表

<img src="https://imgpp.com/s1/2022/06/10/image-20220610163623696.png" alt="image-20220610163623696.png" border="0">

用户临时表  --- 存储未进行邮件激活的用户

<img src="https://imgpp.com/s1/2022/06/10/image-20220610163713317.png" alt="image-20220610163713317.png" border="0">

分类表  --- 存储菜品的分类数据

<img src="https://imgpp.com/s1/2022/06/10/image-20220610163811046.png" alt="image-20220610163811046.png" border="0">

菜品表

<img src="https://imgpp.com/s1/2022/06/10/image-20220610163833781.png" alt="image-20220610163833781.png" border="0">

订单表

<img src="https://imgpp.com/s1/2022/06/10/image-20220610163944143.png" alt="image-20220610163944143.png" border="0">



#### 二、项目环境前置说明

​			为了读者便于理解后期的相关操作，这里列出项目的管理类。

##### 项目统一结果返回类  Result

```java
package com.work.mywork.common;

import lombok.Data;

@Data
public class Result {
    private Integer code;
    private String msg;
    private Object data;

    public Result() {
    }

    public Result(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Result(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
```



##### 项目状态码  Code

```java
package com.work.mywork.common;

public class Code {

    public static final Integer LOGIN_SUCCESS = 10000;
    public static final Integer LOGIN_ERROR = 10001;

    public static final Integer USER_SUCCESS = 20000;
    public static final Integer USER_ERROR = 20001;

    public static final Integer CATEGORY_SUCCESS = 30000;
    public static final Integer CATEGORY_ERROR = 30001;
    
    public static final Integer DISH_SUCCESS = 40000;
    public static final Integer DISH_ERROR = 40001;
    
    public static final Integer TOKEN_ERROR = 401;
}
```



##### 项目异常统一管理类  ProjectException

```java
@RestControllerAdvice
@Slf4j
public class ProjectException {

    @ExceptionHandler(MailSendException.class)
    public Result smtpAddressFailedException(MailSendException exception){
        log.info("邮件异常");
        return new Result(Code.USER_ERROR,"邮件发送失败");
    }

}
```



##### 项目过滤器  CheckLogin  --- 在启动类中开启@ServletComponentScan注解

```java
@WebFilter(filterName = "myfilter",value = "/*")
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CheckLogin implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        // 1.向下转型
        HttpServletRequest servletRequest =  (HttpServletRequest) request;
        HttpServletResponse servletResponse =  (HttpServletResponse) response;

        log.info("token:{}",servletRequest.getHeader("token"));

        // 解决跨域问题
        servletResponse.setHeader("Access-Control-Allow-Origin", servletRequest.getHeader("Origin"));
        servletResponse.setHeader("Access-Control-Allow-Methods", "*");
        servletResponse.setHeader("Access-Control-Allow-Headers", "*");
        servletResponse.setHeader("Access-Control-Allow-Credentials", "true");
        servletResponse.setHeader("Access-Control-Expose-Headers", "*");


        String requestURI = servletRequest.getRequestURI(); // 请求的uri
        log.info("本次请求的uri为：{}",requestURI);
        log.info("本次请求的方法为：{}",servletRequest.getMethod());

        // 操作者id
        String operator = servletRequest.getHeader("operator");
        BaseContext.setId(operator);

        // 2.定义放行url
        String urls [] = new String[]{
                "/api/login",
                "/file/upload",
                "/file/download",
                "/tmp"
        };

        // 3.路由匹配
        AntPathMatcher antPathMatcher = new AntPathMatcher();

        boolean flag = false;
        for (String url : urls) {
            if (antPathMatcher.match(requestURI, url)){
                flag = true;
                break;
            }
        }

        if (flag){
            log.info("路由匹配成功，放行");
            chain.doFilter(servletRequest, servletResponse);
            return;
        }

        log.info("需要对token进行验证");
        Boolean res = JwtUtils.decode(servletRequest.getHeader("token"));
        if (res){
            log.info("token验证成功~");
            chain.doFilter(servletRequest, servletResponse);
            return;
        }

        //token验证失败
        response.getWriter().write(JSON.toJSONString(new Result(Code.TOKEN_ERROR, "NOTLOGIN")));
        return;

    }
}
```



##### 项目公共字段设置类  BaseContext

```java
// 公共字段设置
// 该类在项目中的过滤器中调用，用于存储操作人的id
public class BaseContext {

    public static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static void setId(String id){
        threadLocal.set(id);
    }

    public static String getId(){
       return threadLocal.get();
    }

}
```



##### 项目公共字段填充类  PublicFields

```java
// 公共字段填充
// 统一的设置一些公共字段
@Slf4j
@Configuration
public class PublicFields implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("公共字段填充 --- insert");
        metaObject.setValue("createUser",BaseContext.getId());
        metaObject.setValue("updateUser",BaseContext.getId());
        metaObject.setValue("createTime",LocalDateTime.now());
        metaObject.setValue("updateTime",LocalDateTime.now());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("公共字段填充 --- update");
        metaObject.setValue("updateUser",BaseContext.getId());
        metaObject.setValue("updateTime",LocalDateTime.now());
    }
}
```



##### 配置项目对象映射器 JacksonObjectMapper  --- 提高数据精度

```java
/**
 * 对象映射器:基于jackson将Java对象转为json，或者将json转为Java对象
 * 将JSON解析为Java对象的过程称为 [从JSON反序列化Java对象]
 * 从Java对象生成JSON的过程称为 [序列化Java对象到JSON]
 */
public class JacksonObjectMapper extends ObjectMapper {

    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";

    public JacksonObjectMapper() {
        super();
        //收到未知属性时不报异常
        this.configure(FAIL_ON_UNKNOWN_PROPERTIES, false);

        //反序列化时，属性不存在的兼容处理
        this.getDeserializationConfig().withoutFeatures(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);


        SimpleModule simpleModule = new SimpleModule()
                .addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(DEFAULT_DATE_TIME_FORMAT)))
                .addDeserializer(LocalDate.class, new LocalDateDeserializer(DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT)))
                .addDeserializer(LocalTime.class, new LocalTimeDeserializer(DateTimeFormatter.ofPattern(DEFAULT_TIME_FORMAT)))

                .addSerializer(BigInteger.class, ToStringSerializer.instance)
                .addSerializer(Long.class, ToStringSerializer.instance)
                .addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(DEFAULT_DATE_TIME_FORMAT)))
                .addSerializer(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT)))
                .addSerializer(LocalTime.class, new LocalTimeSerializer(DateTimeFormatter.ofPattern(DEFAULT_TIME_FORMAT)));

        //注册功能模块 例如，可以添加自定义序列化器和反序列化器
        this.registerModule(simpleModule);
    }
}
```



##### 在WebMvc中使用配置好的消息转换器  WebMvcConfig

```java
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        // 创建消息转换器
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        // 设置对象转换器，底层使用Jackson将Java对象转换为JSON
        messageConverter.setObjectMapper(new JacksonObjectMapper());
        // 将上面的消息转换器对象追加到mvc框架的转换器集合中
        // 0代表放到最前面
        converters.add(0,messageConverter);
    }
}
```



##### 项目分页配置  MyBatisPlusConfig

```java
@Configuration
public class MyBatisPlusConfig {
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(){
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return mybatisPlusInterceptor;
    }
}
```



##### Token生成类  JwtUtils

```java
@Slf4j
public class JwtUtils {

    private static final long EXPERT = 60 * 60 * 24 * 1000; // 加密时间
    private static final String SECRET = "***************";

    /**
     * 生成token
     * @param id
     * @return
     */
    public  static String getToken(String id){
        log.info("token生成中。。。");
        String token = Jwts.builder()
                .setSubject("getToken")
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + EXPERT))
                .claim("id",id)
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();

        return token;
    }

    /**
     * 验证token时效
     * @param token
     * @return
     */
    public static Boolean decode(String token){

        log.info("token验证中。。。");
        if (StringUtils.isEmpty(token)){
            log.info("token为空");
            return false;
        }

        try{
            // 解密出现异常就说明是无效token
            Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token);
        }catch (Exception exception){
            log.info("token验证失败");
            return false;
        }

        return true;
    }
}
```



#### 三、管理员登录

##### 控制器  AdminController

```java
@RestController
@RequestMapping("/api")
@Slf4j
public class AdminController {

    @Autowired
    AdminServiceImpl adminService;

    /**
     * 管理员登录接口
     * @param admin
     * @return
     */
    @PostMapping("/login")
    public Result login(@RequestBody Admin admin){
        System.out.println(admin);
        log.info("登录方法");

        Admin findAdmin = adminService.getById(admin.getId());

        // 1.查询账号是否存在
        if (findAdmin == null){
            return new Result(Code.LOGIN_ERROR,"账号未注册");
        }

        // 2.密码验证
        String password = DigestUtils.md5DigestAsHex(admin.getPassword().getBytes(StandardCharsets.UTF_8));
        System.out.println(password);
        System.out.println(findAdmin.getPassword());
        if (!password.equals(findAdmin.getPassword())){
            return new Result(Code.LOGIN_ERROR,"密码有误");
        }

        // 3.登录成功，获取token返回
        String token = JwtUtils.getToken(admin.getId());
        Map<String,String> map = new HashMap<>();
        map.put("token",token);
        map.put("name", findAdmin.getName());

        return new Result(Code.LOGIN_SUCCESS,"登录成功",map);
    }

}
```



#### 四、用户管理



<img src="https://imgpp.com/s1/2022/06/10/da281679e1625383fdfd9a56942d7c7c.png" alt="da281679e1625383fdfd9a56942d7c7c.png" border="0">



<img src="https://imgpp.com/s1/2022/06/10/d2185a91483d21b287d5f916fe2d142d.png" alt="d2185a91483d21b287d5f916fe2d142d.png" border="0">



##### 注意：管理员添加用户功能是我写邮件测试的demo，实际开发流程并非如此

<img src="https://imgpp.com/s1/2022/06/10/3d4e7863680d92cf62ce262601652ade.png" alt="3d4e7863680d92cf62ce262601652ade.png" border="0">



##### 控制器  UserController

```java
@RestController
@RequestMapping("/api/user")
@Slf4j
public class UserController {

    @Autowired
    UserServiceImpl userService;
    @Autowired
    UserTmpService userTmpService;

    // 注入邮件工具类，使用其它方法引用会报空指针异常，这点要十分注意
    @Autowired
    MailUtils mailUtils;

    /**
     * 用户管理 --- 分页查询
     * @param page
     * @param pageSize
     * @param keyword
     * @return
     */
    @GetMapping
    public Result getPageData(int page,int pageSize,String keyword){
        log.info("用户分页查询中。。。");
        log.info("页号：{} --- 数量：{} --- 关键词：{}",page,pageSize,keyword);

        Page<User> userPage = new Page<>(page, pageSize);
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.like(!StringUtils.isEmpty(keyword),User::getUserid,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),User::getUsername,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),User::getSex,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),User::getCreateUser,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),User::getUpdateUser,keyword);

        userService.page(userPage,queryWrapper);

        List<Object> userList = new ArrayList<>();

        for (User record : userPage.getRecords()) {
            Map<String,Object> map = new HashMap<>();
            map.put("userid",record.getUserid());
            map.put("username",record.getUsername());
            map.put("sex",record.getSex() == 1?"男":"女");
            map.put("status",record.getStatus());
            map.put("isActive",record.getIsActive() == 0 ? "未激活":"已激活");
            map.put("createUser",record.getCreateUser());
            map.put("updateUser",record.getUpdateUser());
            map.put("createTime",record.getCreateTime());
            map.put("updateTime",record.getUpdateTime());
            map.put("lastTime",record.getLastTime());
            userList.add(map);
        }
        Map<String,Object> map = new HashMap<>();
        map.put("dataList",userList);
        map.put("total",userPage.getTotal());
        return  new Result(Code.USER_SUCCESS,"查询成功",map);
    }

    /**
     * 用户管理 --- 状态修改
     * @param user
     * @return
     */
    @PostMapping
    public Result setStatus(@RequestBody User user){
        log.info("状态修改中。。。");
        boolean flag = userService.updateById(user);
        Integer status = user.getStatus();
        String userid = user.getUserid();
        log.info("账号：{}的状态修改为【{}】",userid,status);
        return flag
                ? new Result(Code.USER_SUCCESS, status==0? "账号【"+userid+"】已被锁定":"账号【"+userid+"】已解锁")
                : new Result(Code.USER_ERROR,"服务异常");
    }

    
    /**
     * 用户添加
     * @param user
     * @return
     */
    @PostMapping("/add")
    public Result registerUser(@RequestBody User user){
        log.info("用户添加中");

        // 1.判断用户是否已经注册
        User serviceById = userService.getById(user.getUserid());
        if (serviceById != null){
            return new Result(Code.USER_ERROR,"该账号已注册");
        }

        // 2.放入用户临时表中存储，等待激活
        UserTmp userTmp = new UserTmp();
        userTmp.setUserid(user.getUserid());
        userTmp.setPassword(user.getPassword());
        userTmp.setUsername(user.getUsername());
        userTmp.setSex(user.getSex());
        userTmp.setActiveTime(LocalDateTime.now().plusMinutes(5)); //五分钟以内有效
        // 使用工具包通过雪花算法生成确认码
        long code = IdUtil.getSnowflakeNextId();
        userTmp.setConfirmCode(code);

        userTmpService.save(userTmp);

        // 3.向用户发送激活文件
        String url = " http://**************/tmp?confirmCode=" + code;
        boolean flag = mailUtils.send(url, user.getUserid());
        if (flag){
            log.info("邮件发送成功");
            return new Result(Code.USER_SUCCESS,"用户注册成功,请在5分钟内通过邮件激活");
        }else {
            log.info("邮件发送失败");
            return new Result(Code.USER_ERROR,"邮件发送失败");
        }

    }

}
```



##### 控制器  UserTmpController   --- 处理邮件激活请求

```java
@RestController
@RequestMapping("/tmp")
@Slf4j
public class UserTmpController {
    @Autowired
    UserTmpServiceImpl userTmpService;

    @Autowired
    UserServiceImpl userService;

    @GetMapping
    public Result confirm(Long confirmCode, HttpServletResponse response) throws IOException {
        log.info("激活中。。。。");
        log.info("确认码：{}", confirmCode);
        // 在用户注册临时表中查找唯一的确认码
        LambdaQueryWrapper<UserTmp> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UserTmp::getConfirmCode, confirmCode);
        UserTmp serviceOne = userTmpService.getOne(queryWrapper);
        if (serviceOne != null){

            // 判断是否超时
            boolean after = LocalDateTime.now().isAfter(serviceOne.getActiveTime());
            if (after){
                return new Result(Code.USER_ERROR,"激活超时，链接已失效");
            }

            // 确认码一致，激活成功
            log.info("确认码一致，激活成功");
            User user = new User();
            user.setUserid(serviceOne.getUserid());
            String password = DigestUtils.md5DigestAsHex(serviceOne.getPassword().getBytes(StandardCharsets.UTF_8));
            user.setUsername(serviceOne.getUsername());
            user.setPassword(password);
            user.setSex(serviceOne.getSex());
            user.setIsActive(1);
            user.setLastTime(LocalDateTime.now());
            BaseContext.setId(serviceOne.getUserid());
            userService.save(user);

            // 删除临时表中的数据
            userTmpService.remove(queryWrapper);
            response.sendRedirect("http://*************");
        }
        return new Result(Code.USER_ERROR,"激活失败");
    }
}
```



##### 邮件发送工具类 

```java
@Component  // 一定要添加管理注解
@Slf4j
public  class MailUtils {

    @Value("${spring.mail.username}")
    private  String adminEmail;

    @Resource
    private   JavaMailSender javaMailSender;

    @Resource
    private   TemplateEngine templateEngine;


    /**
     * 邮件发送
     * @param activeUrl
     * @param userEmail
     * @return
     */
    public  boolean send(String activeUrl,String userEmail){
        log.info("激活地址：{}",activeUrl);
        log.info("用户邮箱：{}",userEmail);
        try {
            // 创建邮件对象
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage,true);
            // 设置邮件主题
            mimeMessageHelper.setSubject("欢迎使用吃了吗系统服务 --- 账号激活");
            // 设置邮件的发送者
            mimeMessageHelper.setFrom(adminEmail);
            // 设置邮件的接收者
            mimeMessageHelper.setTo(userEmail);
            // 设置邮件的发送时间
            mimeMessageHelper.setSentDate(new Date());
            // 创建上下文环境对象
            Context context = new Context();
            context.setVariable("activeUrl",activeUrl);
            // 将内容放入模板中
            String text = templateEngine.process("active.html",context);

            // 内容装载
            mimeMessageHelper.setText(text,true);

            // 邮件发送
            javaMailSender.send(mimeMessage);
        } catch (MessagingException e){
            return false;
        }

        return true;
    }

}
```



##### 这里简单聊一下我做的邮件发送，我这里使用了网易的邮箱作为代理，然后通过开启SMTP服务、相关授权服务，简单配置一下就可以使用邮件发送功能了，不一定非得用网易的，腾讯的也可以，只要开了服务了就行。服务开启后，会有一个授权码，相当于一个访问令牌，要保管好，详细使用请看我写的源码。

<img src="https://imgpp.com/s1/2022/06/10/image-20220610205830669.png" alt="image-20220610205830669.png" border="0">

<img src="https://imgpp.com/s1/2022/06/10/image-20220610205520641.png" alt="image-20220610205520641.png" border="0">



#### 四、菜品管理

​				时序图这里不再赘述了。

##### 控制器  DishController

```java
@RestController
@RequestMapping("/api/dish")
@Slf4j
public class DishController {

    @Autowired
    DishServiceImpl dishService;
    @Autowired
    CategoryServiceImpl categoryService;

    /**
     * 菜品分页查询
     * @param page
     * @param pageSize
     * @param keyword
     * @return
     */
    @GetMapping
    public Result getPageData(int page,int pageSize,String keyword){
        log.info("分类分页查询中。。。");
        log.info("页号：{} --- 数量：{} --- 关键词：{}",page,pageSize,keyword);

        Page<Dish> dishPage = new Page<>(page, pageSize);
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();

        // 分类名称模糊查询
        if (!StringUtils.isEmpty(keyword)){
            log.info("菜品分类名称反向查询中。。。");
            LambdaQueryWrapper<Category> categoryLambdaQueryWrapper = new LambdaQueryWrapper<>();
            categoryLambdaQueryWrapper.like(Category::getName,keyword);
            List<Map<String, Object>> maps = categoryService.listMaps(categoryLambdaQueryWrapper);
            for (Map<String, Object> map : maps) {
                queryWrapper.eq(Dish::getIsDeleted,0).eq(Dish::getCategoryId,map.get("id")).or();
            }
        }

        queryWrapper
                .eq(Dish::getIsDeleted,0)  // 查询未被删除的
                .like(!StringUtils.isEmpty(keyword),Dish::getName,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),Dish::getCreateUser,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),Dish::getUpdateUser,keyword)
                .or()
                .orderByAsc(Dish::getSort)
                .orderByDesc(Dish::getCreateTime);

        dishService.page(dishPage,queryWrapper);

        // 菜品类名查询
        Page<DishDto> dishDtoPage = new Page<>(); // 保存查询最终结果

        BeanUtils.copyProperties(dishPage,dishDtoPage,"records"); //先保存其它的属性

        List<Dish> records = dishPage.getRecords(); // 单独处理数据列

        List<DishDto> dishDtoList = new ArrayList<>(); // 临时存储

        for (Dish record : records) {
            DishDto dishDto = new DishDto();

            BeanUtils.copyProperties(record,dishDto); // 其余数据迁移

            Long categoryId = record.getCategoryId(); // 查询类名
            Category category = categoryService.getById(categoryId);
            dishDto.setCategoryName(category.getName());

            dishDtoList.add(dishDto);
        }

        dishDtoPage.setRecords(dishDtoList);

        Map<String,Object> map = new HashMap<>();
        map.put("dataList", dishDtoPage.getRecords());
        map.put("total", dishDtoPage.getTotal());

        return new Result(Code.DISH_SUCCESS,"查询成功",map);
    }

    /**
     * 菜品添加、编辑
     * @param dish
     * @return
     */
    @PostMapping
    public Result add(@RequestBody Dish dish){

        log.info(dish+"");
        // 判断是否为编辑操作
        if (dish.getId() != null){
            log.info("菜品编辑");
            // 判断该名称是否被删除，删除则进行恢复
            LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
            dishLambdaQueryWrapper
                    .eq(Dish::getName,dish.getName())
                    .eq(Dish::getIsDeleted,1);

            Dish one = dishService.getOne(dishLambdaQueryWrapper);
            if (one != null){
                one.setIsDeleted(0);
                dishService.updateById(one);
                return new Result(Code.DISH_SUCCESS,"菜品【"+one.getName()+"】恢复成功");
            }

            // 判断新的菜品名称是否存在
            dishLambdaQueryWrapper.clear();
            dishLambdaQueryWrapper
                    .eq(Dish::getIsDeleted,0)
                    .eq(Dish::getName,dish.getName())
                    .notLike(Dish::getId,dish.getId()); // 不对自己进行查询
            Dish serviceOne = dishService.getOne(dishLambdaQueryWrapper);
            if (serviceOne != null){
                return new Result(Code.DISH_ERROR,"菜品【"+serviceOne.getName()+"】已经存在");
            }

            dishService.updateById(dish);
            return  new Result(Code.DISH_SUCCESS,"菜品修改成功");
        }

        // 判断分类名称是否存在
        LambdaQueryWrapper<Category> categoryLambdaQueryWrapper = new LambdaQueryWrapper<>();
        categoryLambdaQueryWrapper
                .eq(Category::getId,dish.getCategoryId())
                .eq(Category::getIsDeleted,0);
        int count = categoryService.count(categoryLambdaQueryWrapper);
        log.info(count+"");
        if (count == 0){
            return new Result(Code.DISH_ERROR,"菜品不存在");
        }

        // 判断菜品是否存在
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Dish::getName,dish.getName());
        Dish serviceOne = dishService.getOne(queryWrapper);

        // 菜品如果存在，检测是否被删除
        if (serviceOne!=null && serviceOne.getIsDeleted() == 1){
            // 被删除，恢复该菜品
            serviceOne.setIsDeleted(0);
            dishService.updateById(serviceOne);
            return new Result(Code.DISH_SUCCESS,"菜品【"+dish.getName()+"】恢复成功");
        }

        // 判断菜品名是否冲突
        if (serviceOne != null){
            return new Result(Code.DISH_ERROR,"菜品【"+dish.getName()+"】已经存在");
        }
        // 添加
        dishService.save(dish);

        return new Result(Code.DISH_SUCCESS,"添加成功");
    }

    /**
     * 菜品删除
     * @param id
     * @return
     */
    @DeleteMapping
    public Result del(Long id) {
        log.info(id + "");

        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Dish::getId, id);

        Dish serviceOne = dishService.getOne(queryWrapper);

        serviceOne.setIsDeleted(1);
        boolean flag = dishService.updateById(serviceOne);
        if (!flag){
            return new Result(Code.DISH_ERROR, "删除异常");
        }
        return new Result(Code.DISH_SUCCESS, "菜品【" + serviceOne.getName() + "】删除成功");
    }

    /**
     * 菜品状态修改
     * @param dish
     * @return
     */
    @PutMapping
    public Result setStatus(@RequestBody Dish dish) {
        log.info("菜品状态更改为：{}",dish.getStatus());

        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Dish::getId, dish.getId());

        Dish serviceOne = dishService.getOne(queryWrapper);

        serviceOne.setStatus(dish.getStatus());
        boolean flag = dishService.updateById(serviceOne);
        if (!flag){
            return new Result(Code.DISH_ERROR, "状态更改时发送异常");
        }
        return new Result(Code.DISH_SUCCESS, "菜品【" + serviceOne.getName() + "】状态更改成功");
    }

}
```



##### DishDto  用于菜品反向查询类名

```java
@Data
public class DishDto extends Dish {
    private String categoryName;
}
```



##### FileController  用于处理菜品图片上传与下载请求

```java
@RestController
@Slf4j
@RequestMapping("/file")
// TODO 文件上传下载实现
public class FileController {

    @Value("${image.path}")
    private String basePath;

    /**
     * 必须使用POST方法上传
     * 接收参数名必须和前端发送的名字一样
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public Result upload(MultipartFile file){
        log.info("图片文件存储中。。。");

        // 获取原始文件名
        String originalFilename = file.getOriginalFilename();

        // 截取文件后缀
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        log.info("文件后缀：{}",suffix);

        // 随机生成文件名
        UUID imgId = UUID.randomUUID();

        // 生成的文件名
        String filename = imgId+suffix;
        log.info("生成的文件名：{}",filename);

        // 将该文件存储到目录中
        File dir = new File(basePath);

        // 判断目录是否存在
        if (!dir.exists()){
            dir.mkdir();
        }

        // 将文件从临时目录转存至指定目录下
        try{
            file.transferTo(new File(basePath + filename));
        }catch (IOException ex){
            ex.getMessage();
        }

        // 将生成的文件名返给前端
        Map<String,String> map = new HashMap<>();
        map.put("filename",filename);
        return new Result(Code.DISH_SUCCESS,"图片文件上传成功",map);

    }


    /**
     * 必须使用GET方法下载
     * 接收参数名必须和前端发送的名字一样
     * @param filename
     * @param response
     */
    @GetMapping("/download")
    public void download(String filename, HttpServletResponse response){
        log.info("图片下载中。。。");
        log.info(filename);
        try{
            // 通过输入流读取指定的文件
            FileInputStream fileInputStream = new FileInputStream(basePath+ filename);

            // 通过输出流写入到浏览器中进行展示
            ServletOutputStream servletOutputStream = response.getOutputStream();
            //设置返回的数据类型
            response.setContentType("image/jpeg");

            int len = 0;
            byte bytes [] = new byte[1024];
            while ( (len = fileInputStream.read(bytes)) != -1){
                servletOutputStream.write(bytes,0,len);  // 通过输出流向页面写入存储的图片
                servletOutputStream.flush();
            }

            fileInputStream.close();
            servletOutputStream.close();
        }catch (FileNotFoundException e){
            e.getMessage();
        }catch (IOException e){
            e.getMessage();
        }

    }

}
```



#### 五、分类管理

##### 控制器  CategoryController

```java
@RestController
@Slf4j
@RequestMapping("/api/category")
public class CategoryController {

    @Autowired
    CategoryServiceImpl categoryService;

    @Autowired
    DishServiceImpl dishService;

    /**
     * 分类分页查询
     * @param page
     * @param pageSize
     * @param keyword
     * @return
     */
    @GetMapping
    public Result getPageData(int page,int pageSize,String keyword){
        log.info("分类分页查询中。。。");
        log.info("页号：{} --- 数量：{} --- 关键词：{}",page,pageSize,keyword);

        Page<Category> categoryPage = new Page<>(page, pageSize);
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper
                .eq(Category::getIsDeleted,0)  // 查询未被删除的
                .like(!StringUtils.isEmpty(keyword),Category::getName,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),Category::getSort,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),Category::getCreateUser,keyword)
                .or()
                .like(!StringUtils.isEmpty(keyword),Category::getUpdateUser,keyword)
                .orderByAsc(Category::getSort)
                .orderByDesc(Category::getCreateTime);

        categoryService.page(categoryPage,queryWrapper);

        Map<String,Object> map = new HashMap<>();
        map.put("dataList",categoryPage.getRecords());
        map.put("total",categoryPage.getTotal());

        return new Result(Code.CATEGORY_SUCCESS,"查询成功",map);
    }

    /**
     * 分类添加、编辑
     * @param category
     * @return
     */
    @PostMapping
    public Result categoryAdd(@RequestBody Category category){
        log.info("分类添加/编辑中。。。");

        // 判断是添加还是编辑操作,id不为空则是编辑操作
        if (category.getId() != null){
            LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();

            // 判断编辑的名称是否被删除，如果被删除则恢复，当前的分类状态被转为删除
            queryWrapper
                    .eq(Category::getName,category.getName())
                    .eq(Category::getIsDeleted,1);
            Category one = categoryService.getOne(queryWrapper);
            if (one != null) {
                // 检测到是过往删除的分类，进行恢复
                one.setIsDeleted(0);
                categoryService.updateById(one);
                return  new Result(Code.CATEGORY_SUCCESS,"分类【"+category.getName()+"】恢复成功");
            }

            // 判断新的名称是否存在
            queryWrapper.clear();
            queryWrapper
                    .eq(Category::getIsDeleted,0)  // 查询未被删除的
                    .eq(Category::getName,category.getName())
                    .notLike(Category::getId,category.getId());
            Category serviceOne = categoryService.getOne(queryWrapper);
            if (serviceOne != null){
                return new Result(Code.CATEGORY_ERROR,"分类【"+category.getName()+"】已经存在");
            }
            categoryService.updateById(category);
            return  new Result(Code.CATEGORY_SUCCESS,"分类修改成功");
        }

        // 判断添加的分类名是否被逻辑删除，如果被删除则恢复它
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                .eq(Category::getName,category.getName())
                .eq(Category::getIsDeleted,1);

        Category categoryServiceOne = categoryService.getOne(queryWrapper);
        if (categoryServiceOne!=null){
            categoryServiceOne.setIsDeleted(0);
            categoryService.updateById(categoryServiceOne);
            return  new Result(Code.CATEGORY_SUCCESS,"分类【"+category.getName()+"】恢复成功");
        }

        // 判断分类名是否已经存在
        queryWrapper.clear();
        queryWrapper
                .eq(Category::getIsDeleted,0)  // 查询未被删除的
                .eq(Category::getName,category.getName());
        int count= categoryService.count(queryWrapper);
        if (count > 0){
            return new Result(Code.CATEGORY_ERROR,"分类【"+category.getName()+"】已经存在");
        }

        categoryService.save(category);

        return new Result(Code.CATEGORY_SUCCESS,"分类添加成功");
    }

    /**
     * 分类编辑前置查询
     * @param id
     * @return
     */
    @GetMapping("/one")
    public Result getOneData(Long id){
        Category category = categoryService.getById(id);
        System.out.println(category);

        Map<String,Object> map = new HashMap<>();
        map.put("name",category.getName());
        map.put("sort",category.getSort());

        return new Result(Code.CATEGORY_SUCCESS,"查询成功",map);
    }

    /**
     * 分类删除
     * @param id
     * @return
     */
    @DeleteMapping
    public Result delete(Long id){
        log.info("分类删除中。。。");

        Category category = categoryService.getById(id);

        // 判断该分类是否已经绑定了菜品，如果绑定了就不允许删除
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.eq(Dish::getCategoryId,id);
        int count = dishService.count(dishLambdaQueryWrapper);  // 注意，在这里不能用getOne()
        if (count > 0){
            return new Result(Code.CATEGORY_ERROR,"分类【"+category.getName()+"】已经绑定了菜品，无法删除!");
        }

        category.setIsDeleted(1);
        boolean flag = categoryService.updateById(category);
        if (!flag){
            return new Result(Code.CATEGORY_ERROR,"删除异常");
        }

        return new Result(Code.CATEGORY_SUCCESS,"分类【"+category.getName()+"】删除成功");
    }

    /**
     * 获取所有分类
     * @return
     */
    @GetMapping("/getallname")
    public Result getAllName(){

        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Category::getIsDeleted,0);

        List<Map<String, Object>> mapList = categoryService.listMaps(queryWrapper);

        return new Result(Code.CATEGORY_SUCCESS,"分类名称查询成功",mapList);

    }
```

